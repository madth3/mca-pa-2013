package mca.pa2013.clase01

def sum = 0
StringBuilder sb = new StringBuilder()

(0..1000).each {k ->
    if ((k % 3 == 0) || (k % 5 == 0))
        sum += k
        sb.append(k.toString())
}
println sum
println sb

// Version de una sola línea
println ((0..1000).findAll({ k-> return (k % 3 == 0) || (k % 5 == 0)}).sum())

