package mca.pa2013.clase01

def list = [4,6,8,'a','b','c']
println list.class

println list[-1]
println list[0..-2]

def list2 = 0..10
println list2
println 0..<10

def list3 = list + list2
println list3

println list3.sum()
println list2.sum()

//list = [4,6,8,'a','b','c']
list[4] = 'B'
list[2..2] = []
println list
list[2..-1] = [8,2,6,1,9,3]
println list

list.each{ a->
    if (a % 2 == 0) print a
}

println list.findAll{ a->
    return a % 2 == 0
}



