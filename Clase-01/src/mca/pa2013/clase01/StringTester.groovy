package mca.pa2013.clase01

def name='Julio'

def saludo = 'Hello $name!'

println saludo[0..5]
println saludo[-1]

def sb = new StringBuilder()
sb << saludo
sb << ' and Cesar!'
println sb
println saludo.length()
println saludo.indexOf(' ')
println saludo.split('')
println saludo.split('').join('_')

