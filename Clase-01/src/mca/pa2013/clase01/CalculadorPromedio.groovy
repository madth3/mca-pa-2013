package mca.pa2013.clase01

def entrada = new Scanner(System.in)

println "Escribe en una línea los números cuyo promedio se desea calcular."
def numeros = []
def suma = 0.0
def contador = 0

def linea = entrada.nextLine()

linea.split("\\s+").each {numero ->
    numeros[contador] = Float.valueOf(numero)
    suma += numeros[contador]
    contador++
}

println suma/numeros.size