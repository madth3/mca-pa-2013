package mca.pa2013.spark

import javax.persistence.Persistence
import spark.*
import mca.pa2013.datos.ProductJpaController
import groovy.json.JsonBuilder

def route(String ruta, Closure handler) {
    return new Route(ruta) {
        public Object handle(Request req, Response resp) {
            return handler.call(req, resp)
        }
    }
}

Spark.get route('/hola', {req, resp -> 'Hola mundo'})

Spark.get route(
    '/sumar',
    {req, resp -> 
        def a = Long.valueOf(req.queryParams('a'))
        def b = Long.valueOf(req.queryParams('b'))
        return "El resultado es <code>${a+b}</code>"
    }
)

def emf = Persistence.createEntityManagerFactory('EjemploSparkPU')
def productoDao = new ProductJpaController(emf)
Spark.get route(
    '/productos',
    {req, resp -> 
        return productoDao.findProductEntities().collect{it.description}.toString()
    }
)

Spark.get route(
    '/productos.json',
    {req, resp -> 
        def productos = productoDao.findProductEntities().collect{
            [id: it.productId,
            nombre: it.description,
            costo: it.purchaseCost]
        }
        resp.type 'application/json'
        resp.header('Access-Control-Allow-Origin', '*')
        def json = new JsonBuilder(productos)
        return json.toPrettyString()
    }
)

//Spark.get route(
//    '/productos.html',
//    {req, resp ->
//        return new File('web/productos.html').text
//    }
//)




