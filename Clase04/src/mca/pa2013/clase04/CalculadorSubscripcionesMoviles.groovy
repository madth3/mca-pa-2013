package mca.pa2013.clase04

import org.apache.poi.ss.usermodel.*

def workbook = WorkbookFactory.create(new FileInputStream("World Bank Indicators.xlsx"))

def hoja = workbook.getSheetAt(0)
def numFilas = hoja.getLastRowNum()

def mapaNumerosSubscriptores = [:]

(1..numFilas).each { k->
    def renglon = hoja.getRow(k)
    
    def pais = renglon.getCell(0).stringCellValue
    def fecha = renglon.getCell(1).dateCellValue
    def numSubscriptores = -1
    if (renglon.getCell(4) != null) {
        numSubscriptores = renglon.getCell(4).numericCellValue
    }
    switch(fecha.getYear() + 1900) {
        case 2009:
            mapaNumerosSubscriptores[pais] = [numSubscriptores]
            break;
        case 2010:
            def nums = mapaNumerosSubscriptores[pais]
            if (nums[0] != -1 && numSubscriptores != -1) {
                nums << numSubscriptores
                nums << nums[1]*1.0/nums[0]
            }
            break;
    }    
}

mapaNumerosSubscriptores.each{ pais, datos ->
    if (datos.size() == 3) {
        println "$pais: ${(datos[-1]-1) * 100}"
    }
}







