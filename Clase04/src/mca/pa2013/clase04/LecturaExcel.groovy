package mca.pa2013.clase04

import org.apache.poi.ss.usermodel.*

def workbook = WorkbookFactory.create(new FileInputStream("World Bank Indicators.xlsx"))

println "${workbook.numberOfSheets} hojas"

def hoja = workbook.getSheetAt(1)
def numFilas = hoja.getLastRowNum()

//Map<String, List<String>> mapaPaisesPorRegion
def mapaPaisesPorRegion = [:]

(1..numFilas).each { k ->
    def renglon = hoja.getRow(k)
    def region = renglon.getCell(0).stringCellValue
    def pais = renglon.getCell(2).stringCellValue
    if (mapaPaisesPorRegion.containsKey(region)) {
        mapaPaisesPorRegion[region] << pais
    }
    else {
        mapaPaisesPorRegion[region] = [pais]
    }
}

def listaRegiones = mapaPaisesPorRegion.entrySet().asList()
listaRegiones.sort{ -it.value.size() }
listaRegiones.each { entry ->
    println "${entry.key} - ${entry.value.size()}: ${entry.value}"
}

//mapaPaisesPorRegion.each { region, numero ->
//    println "$region: $numero"
//}








