package mca.pa2013.concurrencia

import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

def NUM_HILOS = 10
def TAMANIO_POOL = 5

def servicioEjecutor = Executors.newFixedThreadPool(TAMANIO_POOL)
for (i in 0..<NUM_HILOS) {
    servicioEjecutor.execute(new GeneradorCadenas(String.valueOf((('A' as char) + i) as char)))
}
servicioEjecutor.shutdown()
servicioEjecutor.awaitTermination(120, TimeUnit.SECONDS)
println GeneradorCadenas.sbGlobal.toString()

// Ejemplos de variables Thread-Safe
def map = [:]
def mapSeguro = new Hashtable()

def lista = []
def listaSegura = new Vector()


