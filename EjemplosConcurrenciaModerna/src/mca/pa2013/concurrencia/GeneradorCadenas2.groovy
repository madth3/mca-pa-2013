package mca.pa2013.concurrencia

/**
 *
 * @author PCEL
 */
class GeneradorCadenas2 implements Runnable {
    int contadorInterno
    int limiteInterno
    String charInterno
    boolean enSilencio
    static StringBuilder sbGlobal = new StringBuilder()
    
    public GeneradorCadenas2(c, silencio = false, limite = 10) {
        charInterno = c
        enSilencio = silencio
        limiteInterno = limite
        if (!enSilencio) {
            mensaje("Generador ${charInterno}")
        }
    }
    
    public mensaje(s) {
        println "[${Thread.currentThread().getName()}}] ${s}"
    }
    
    public void run() {
        Random rand = new Random()
        try {
            for (contadorInterno = 1; contadorInterno <= limiteInterno; contadorInterno++) {
                //Thread.sleep(200 + rand.nextInt(500))
                sbGlobal.append("[")
                sbGlobal.append(charInterno * contadorInterno)
                sbGlobal.append("]")
                if (!enSilencio) {
                    mensaje("${charInterno}${contadorInterno} => ${sbGlobal.toString()}")
                }
            }
        }
        catch (InterruptedException ie) {
            mensaje("Interrupted")
        }
    }
}

