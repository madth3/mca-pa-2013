package mca.pa2013.concurrencia

import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

def balanceado(cadena) {
    def abierto = false
    for (c in cadena) {
        if (c == '[') {
            if (abierto) return false
            else {
                abierto = true
            }
        }
        if (c == ']') { 
            if (!abierto) return false
            else {
                abierto = false
            }
        }
    }
    return true
}

def NUM_HILOS = 10
def TAMANIO_POOL = 5
for (k in 1..2000) {
    GeneradorCadenas.sbGlobal = new StringBuilder()
    def servicioEjecutor = Executors.newFixedThreadPool(TAMANIO_POOL)
    for (i in 0..<NUM_HILOS) {
        servicioEjecutor.execute(new GeneradorCadenas(String.valueOf((('A' as char) + i) as char), true, 100))
    }
    servicioEjecutor.shutdown()
    servicioEjecutor.awaitTermination(120, TimeUnit.SECONDS)
    def resultado = GeneradorCadenas.sbGlobal.toString()
    if (!balanceado(resultado)) { println resultado}
}

// Ejemplos de salidas
//[C][CC][CCC][CCCC][CCCCC][B][BB][ [BBB D] ][[EBED] [B ]B[]E EDE] [D ]B[BEB] E D ]D[D][EAE]D[ DEDE]A DA]][AAA][AAAA][AAAAA]
//[A][AA][AAA][AAAA][AAAAA][D][DD][DDD][DDDD][DDDDD][B][BB][BBB] [BEB]B[B][EE]B[BBBEB]EE][EEEE][EEEEE][C][CC][CCC][CCCC][CCCCC]
//[A][AA][AAA][AAAA][AAAAA][B][BB][BBB][BBBB][BBBBB][C][CC][CCC][CCCC][CCCCC][[D][E][ D][EE]D[DD][ EE]D[DDD] [EEE] [DDDD ]EEEE]
//[A][AA][AAA][AAAA][AAAAA][B][BB][BBB][BBBB][BB[BBB]C][CC][CCC][CCCC][CCCCC][D][DD][DDD][DDDD][DDDDD][E][EE][EEE][EEEE][EEEEE]
//[A][AA[][ABA]A[] BB][AAAA]B[BB][AAABABAB]B][BBBBB][C][CC][CCC][CCCC][CCCCC][D][DD][DDD][DDDD][DDDDD][E][EE][EEE][EEEE][EEEEE]
//[A][AA][AAA][AAAA][AAAAA][B][BB][BBB][BBBB][BB[BBB]C][CC][CCC][CCCC][CCCCC][D][DD][DDD][DDDD][DDDDD][E][EE][EEE][EEEE][EEEEE]
//[A][AA][AAA][AAAA][AAAAA][B][BB][BBB][[BBBB][C][BBBBCB]C][CCC][CCCC][CCCCC][D][DD][DDD][DDDD][DDDDD][E][EE][EEE][EEEE][EEEEE]