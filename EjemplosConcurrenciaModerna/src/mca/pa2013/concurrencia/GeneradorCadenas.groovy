package mca.pa2013.concurrencia

/**
 *
 * @author PCEL
 */
class GeneradorCadenas implements Runnable {
    int contadorInterno
    int limiteInterno
    String charInterno
    boolean enSilencio
    //static StringBuilder sbGlobal = new StringBuilder()
    static StringBuffer sbGlobal = new StringBuffer()
    
    public GeneradorCadenas(c, silencio = false, limite = 10) {
        charInterno = c
        enSilencio = silencio
        limiteInterno = limite
        if (!enSilencio) {
            mensaje("Generador ${charInterno}")
        }
    }
    
    public mensaje(s) {
        println "[${Thread.currentThread().getName()}}] ${s}"
    }
    
    public void run() {
        //Random rand = new Random()
        try {
            for (contadorInterno in 1..limiteInterno) {
                //Thread.sleep(200 + rand.nextInt(500))
                /*
                sbGlobal.append("[")
                for (i in 1..contadorInterno) {
                    sbGlobal.append(charInterno)
                }
                sbGlobal.append("]")
                */
                // Alternativa
                sbGlobal.append("[${charInterno*contadorInterno}]")
                if (!enSilencio) {
                    mensaje("${charInterno}${contadorInterno} => ${sbGlobal.toString()}")
                }
            }
        }
        catch (InterruptedException ie) {
            mensaje("Interrupted")
        }
    }
}

