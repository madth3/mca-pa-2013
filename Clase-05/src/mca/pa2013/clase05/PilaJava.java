package mca.pa2013.clase05;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jaguilar
 */
public class PilaJava<T> {

    private List<T> lista = new ArrayList();

    public void push(T elem) {
        lista.add(elem);
    }

    public T pop() throws IllegalAccessException {
        if (lista.size() > 0) {
            T elem = lista.get(lista.size() - 1);
            lista.remove(lista.size() - 1);
            return elem;
        }
        else {
            throw new IllegalAccessException("Pila vacia");
        }
    }
    
    public int size() {
        return lista.size();
    }
}
