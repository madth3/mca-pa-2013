package mca.pa2013.clase05

def movimientosTorresHanoi(n, posteInicial, posteFinal, posteAuxiliar) {
    if (n == 1) {
        def disco = posteInicial.pop()
        posteFinal.push(disco)
        println "$disco: ${posteInicial.nombre} -> ${posteFinal.nombre}"
    }
    else {
        movimientosTorresHanoi(n-1, posteInicial, posteAuxiliar, posteFinal)
        def disco = posteInicial.pop()
        posteFinal.push(disco)
        println "$disco: ${posteInicial.nombre} -> ${posteFinal.nombre}"
        movimientosTorresHanoi(n-1, posteAuxiliar, posteFinal, posteInicial)
    }
}

def posteA = new Pila(nombre: "A")
def posteB = new Pila(nombre: "B")
def posteC = new Pila(nombre: "C")

def n = 7
(n..1).each { k ->
    posteA.push("Disco $k")
}

movimientosTorresHanoi(n, posteA, posteC, posteB)




