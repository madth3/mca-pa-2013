package mca.pa2013.clase05

def trapecio(f, a, b) { (f(a) + f(b))*(b - a)*0.5 }

def integral(f, a, b, epsilon = 0.01, areaReferencia = 1) {
    def primeraArea = trapecio(f, a, b)
    def m = (a + b)*0.5
    def segundaArea = trapecio(f, a, m) + trapecio(f, m, b)
    if (Math.abs(1 - primeraArea/segundaArea) < epsilon * areaReferencia) {///
        return segundaArea
    }
    else {
        integral(f, a, m, epsilon, segundaArea*0.5) 
      + integral(f, m, b, epsilon, segundaArea*0.5)
    }
}

            
def identidad = { x -> x }
//assert Math.abs(integral(identidad, 0, 10) - 50) < 0.1
println integral({x -> x*x}, 1, 10,0.0001)
//assert Math.abs(integral({x -> x*x}, 0, 10) - 333.333) < 0.1
            

            
            
            
            
            