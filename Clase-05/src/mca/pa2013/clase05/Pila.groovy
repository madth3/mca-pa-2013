package mca.pa2013.clase05

/**
 *
 * @author jaguilar
 */
class Pila {
	
    def lista = []
    String nombre
    
    def push(elem) {
        lista << elem
        //lista.add(elem)
    }
    
    def pop() {
        if (lista.size() > 0) {
            def elem = lista[-1]
            lista.remove(lista.size()-1)
            return elem
        }
        else {
            // return null
            throw new IllegalAccessException("La pila esta vacia")
            // FEO: return []
        }
    }
    
    public int size() {
        return lista.size()
    }
    
}




