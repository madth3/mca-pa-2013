package mca.pa2012.tarea02

def mezclarCadenas(a, b) {
    return ""
}

assert mezclarCadenas("abc", "xyz") == "axbycz"
assert mezclarCadenas("Hi", "There") == "HTihere"
assert mezclarCadenas("xxxx", "There") == "xTxhxexre"
assert mezclarCadenas("xxx", "X") == "xXxx"
assert mezclarCadenas("2/", "27 around") == "22/7 around"
assert mezclarCadenas("", "Hello") == "Hello"
assert mezclarCadenas("Abc", "") == "Abc"
assert mezclarCadenas("", "") == ""
assert mezclarCadenas("a", "b") == "ab"
assert mezclarCadenas("ax", "b") == "abx"
assert mezclarCadenas("a", "bx") == "abx"
assert mezclarCadenas("So", "Long") == "SLoong"
assert mezclarCadenas("Long", "So") == "LSoong"