package mca.pa2012.tarea02;

import java.util.Comparator;

/**
 *
 * @author jaguilar
 */
public class Punto {
    double x;
    double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Punto(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public static Comparator<Punto> comparadorX() {
        return new Comparator<Punto>() {

            @Override
            public int compare(Punto o1, Punto o2) {
                return Double.compare(o1.x, o2.x);
            }
             
        }
    }
}






