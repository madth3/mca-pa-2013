package mca.pa2012.tarea02

def distancia(p1, p2) {
    return Math.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)
}
// La lista puntos ya debe estar ordenada
def distanciaMinima(puntos) {
    if (puntos.size() == 1) return Double.MAX_VALUE
    if (puntos.size() == 2) return distancia(puntos[0], puntos[1])
    def m = puntos.size().intdiv(2)
    def d1 = distanciaMinima(puntos[0..<m])
    def d2 = distanciaMinima(puntos[m..-1])
    def d = Math.min(d1, d2)
    def xm = (puntos[m-1][0] + puntos[m][0]) * 0.5
    def izq = m-1
    while (izq >= 0 && xm - puntos[izq][0] <= d) izq--
    izq++
    def der = m
    while (der < puntos.size() && puntos[der][0] - xm <= d) der++
    der--
    if (izq < m && der >= m) {
    (izq..<m).each{ i ->
        (m..der).each{ j ->
            dc = distancia(puntos[i], puntos[j])
            if (dc < d) d = dc
        }
    }
    }
    return d
}

def scn = new Scanner(new File("puntos01.txt"))
def k = 0
def puntos = []
while (scn.hasNextLine()) {
    def linea = scn.nextLine()
    def punto = linea.split("\\s+").collect{Double.valueOf(it)}
    //def punto = [Double.valueOf(puntoStr[0]), Double.valueOf(puntoStr[1])]
    puntos << punto
    k++
}

puntos.sort{p -> p[0]}
println "Se leyeron $k parejas de puntos"
println "La distancia minima es ${distanciaMinima(puntos)}"



