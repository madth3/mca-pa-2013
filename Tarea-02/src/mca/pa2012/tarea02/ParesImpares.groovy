package mca.pa2012.tarea02

def paresImpares(lista) {
    return []
}

assert paresImpares([]) == []
assert paresImpares([1]) == [1]
assert paresImpares([1, 2]) == [2, 1]
assert paresImpares([2, 1]) == [2, 1]
assert paresImpares([1, 4, 1, 0, 6, 1, 1]) == [4, 0, 6, 1, 1, 1, 1]
assert paresImpares([3, 3, 2]) == [2, 3, 3]
assert paresImpares([2, 2, 2]) == [2, 2, 2]
assert paresImpares([3, 2, 2]) == [2, 2, 3]
assert paresImpares([3, 1, 0, 7, 2]) == [0, 2, 3, 1, 7]