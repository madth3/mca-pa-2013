package mca.pa2012.tarea02

def cicloNormal() {
    for (int i = 0; i < 20; i++) {
        print "= "
        if (i > 10) return
        println i
    }
}

def cicloEach() {
    (0..<20).each { i->
        print "= "
        if (i > 10) return
        println i
    }
}

def cicloCollect() {
    (0..<20).collect { i ->
         if (i % 2 == 0) return i*i
         else return i-1
    }
}

cicloNormal()
cicloEach()