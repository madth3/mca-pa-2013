package mca.pa2013.clase02

def calificaciones = ['Hugo': [8,7,9], 'Paco': [7.5, 8.5, 9.5], 'Luis':[10, 8, 6]]

calificaciones['Luis'][-1] = 7
calificaciones['Paco'] = 'REPROBADO'

println calificaciones
