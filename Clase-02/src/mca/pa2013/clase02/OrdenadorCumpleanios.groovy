package mca.pa2013.clase02

def fechasNacimiento = [:]

fechasNacimiento['Javier'] = new Date(100, 4, 4)
fechasNacimiento['Cristina'] = new Date(100, 6, 23)
fechasNacimiento['Miguel Angel'] = new Date(100,0,12)
fechasNacimiento['Julio'] = new Date(100,10,17)
fechasNacimiento['Edith'] = new Date(100,8,16)
fechasNacimiento['Tania'] = new Date(100,8,16)

def nombres = new ArrayList(fechasNacimiento.keySet())
nombres.sort()
nombres.eachWithIndex{ nombre, k ->
    println k + '. ' + nombre.padRight(20, ' ') + ' ' + fechasNacimiento[nombre]
}

println ''

def fechas = new ArrayList(new HashSet(fechasNacimiento.values()))
fechas.sort()
def k = 1
fechas.each{ f ->
    fechasNacimiento.each{nombre, fecha ->
        if (fecha.equals(f)) {
            println k + '. ' + nombre.padRight(20, ' ') + ' ' + f
            k++
        }
    }
}

