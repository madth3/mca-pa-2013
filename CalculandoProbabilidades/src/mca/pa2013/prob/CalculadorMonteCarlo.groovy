package mca.pa2013.prob

/**
 *
 * @author jaguilar
 */
class CalculadorMonteCarlo {
	int REPETICIONES
    def generador
    def evaluador
    
    public CalculadorMonteCarlo(generador, evaluador, repeticiones) {
        this.generador = generador
        this.evaluador = evaluador
        this.REPETICIONES = repeticiones
    }
    
    public double calcularProbabilidad() {
        def contador = 0
        for (k in 1..REPETICIONES) {
            def caso = generador.generarCaso()
            if (evaluador.evaluar(caso)) contador++ 
        }
        return contador * 1.0 / REPETICIONES
    }
}

