package mca.pa2013.prob

def REPETICIONES = 10000000


def rnd = new Random()

def contador = 0
for (ciclo in 1..REPETICIONES) {
    def cumpleanios = []
    
    for (k in 1..3) {
        cumpleanios << rnd.nextInt(365)
    }
    
//    def maxC = cumpleanios.max()
//    def minC = cumpleanios.min()
    cumpleanios.sort()
    def maxC = cumpleanios[2]
    def minC = cumpleanios[0]
    def medioC = cumpleanios[1]
    
    if (maxC - minC < 7) {
        contador++
    }
    
    if ((maxC - minC > 358) && 
        (medioC - minC > 358 || maxC - medioC > 358)) {
        contador++
    }
    
}

println contador*1.0/REPETICIONES

