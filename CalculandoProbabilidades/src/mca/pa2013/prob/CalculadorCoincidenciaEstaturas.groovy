package mca.pa2013.prob

class GeneradorEstaturas {
    def rnd = new Random()
    
    //def ajustarValor(x) { return 40*x + 150}
    def ajustarValor(x) { return 20*x + 170}
    def generarCaso() {
        return [ajustarValor(rnd.nextGaussian()), ajustarValor(rnd.nextGaussian())]
    }
}