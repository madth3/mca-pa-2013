package mca.pa2013.prob

class GeneradorPuntos {
    def rnd = new Random()
    def generarCaso() { 
        return [rnd.nextDouble(), rnd.nextDouble()] 
    }
}

class EvaluadorInterseccion {
    double R = 0.5 * Math.sqrt(2)
    def distancia(p1, p2) { 
        return Math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2) 
    }
    def evaluar(punto) {
        return distancia(punto,[0,0.5]) <= R && distancia(punto,[1,0.5]) <= R 
    }
}

def calculador = new CalculadorMonteCarlo(
        new GeneradorPuntos(), 
        new EvaluadorInterseccion(), 
        1_000_000)

println calculador.calcularProbabilidad()

