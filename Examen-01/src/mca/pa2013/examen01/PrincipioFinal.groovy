package mca.pa2013.examen01

/**
 * Encuentra la subcadena mas larga (0 o mas caracteres) que aparezca al principio 
 * de la cadena y tambien al final en orden inverso.
 */
def encontrarPrincipioFinal(cadena) {
}

/******************************************************************************/
/**                 NO MODIFICAR DEBAJO DE ESTAS LINEAS                      **/
def runTests(metodo, tests) {
    def correctos = 0
    tests.each{args, esperado ->
        try {
            def resultado = metodo(args[0])
            if (resultado == esperado) correctos++
            else {
                println "Se obtuvo <<$resultado>> y se esperaba <<$esperado>>"
            }
        }
        catch (Exception ex) {
            ex.printStackTrace()
        }
    }
    println "Pasó $correctos tests de ${tests.size()}"
}

casos = [:]
casos[[""]] = ""
casos[["abcdefg"]] = ""
casos[["abXYZba"]] = "ab"
casos[["xxYxx"]] = "xxYxx"
casos[["band andab"]] = "ba"
casos[["123 and then 321"]] = "123 "
casos[["123 ademas  sameda 321"]] = "123 ademas  sameda 321"
casos[["123 Alada 321"]] = "123 "
runTests({s -> encontrarPrincipioFinal(s)}, casos)