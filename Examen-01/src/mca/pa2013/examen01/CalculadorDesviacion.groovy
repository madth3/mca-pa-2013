package mca.pa2013.examen01

/**
 * Calcula la desviación estándar de un conjunto de números.
 * http://en.wikipedia.org/wiki/Standard_deviation#Basic_examples
 * El resultado debe tener al menos tres digitos de precision.
 */
def desviacionEstandar(numeros) {
    return -1
}

/******************************************************************************/
/**                 NO MODIFICAR DEBAJO DE ESTAS LINEAS                      **/
def runTests(metodo, tests) {
    def correctos = 0
    tests.each{args, esperado ->
        try {
            def resultado = metodo(args[0])
            if (Math.abs(resultado - esperado) < 0.001) correctos++
            else {
                println "Se obtuvo <<$resultado>> y se esperaba <<$esperado>>"
            }
        }
        catch (Exception ex) {
            ex.printStackTrace()
        }
    }
    println "Pasó $correctos tests de ${tests.size()}"
}

casos = [:]
casos[[[]]] = 0
casos[[[20,20,20,20,20,20]]] = 0
casos[[1..10 as List]] = 2.87228
def l1 = (1..50 as List) + (1..20 as List)
Collections.shuffle(l1)
casos[[l1]] = 14.28874
casos[[l1*10]] = 14.28874
//casos[[1:1],[2:4,3:6]] = [1:1, 2:4, 3:6]
//casos[[:],[2:4,3:6]] = [2:4,3:6]
//casos[[1:1,2:2],[2:-4,3:-6]] = [1:1, 2:2, 3:-6]
//casos[[1:10,2:20,3:30,4:40],[1:11,2:18,5:16]] = [1:11,2:20,3:30,4:40,5:16]
//casos[[1:10,2:20,3:30,4:40],[1:10,2:20,3:30,4:40]] = [1:10,2:20,3:30,4:40]
//casos[[1:10,2:20,3:30,4:40],[11:11,12:18,15:16]] = [1:10,2:20,3:30,4:40,11:11,12:18,15:16]
//casos[['a':1, 'B':27, 'X':7], ["a":20, "b":28, "Equis": 10]] = ['a':20,'B':27, 'b':28, 'Equis':10, 'X':7]

runTests({l -> desviacionEstandar(l)}, casos)