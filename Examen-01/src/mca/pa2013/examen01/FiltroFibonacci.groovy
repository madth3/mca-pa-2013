package mca.pa2013.examen01

/**
 * El metodo regresa una lista de todos los numeros que están en la lista 
 * inicial y que NO son numeros de Fibonacci. Ademas, la lista resultado
 * debera estar ordenada de menor a mayor
 */
def filtrarFibonaccis(lista) {
    return null
}

/******************************************************************************/
/**                 NO MODIFICAR DEBAJO DE ESTAS LINEAS                      **/
def runTests(metodo, tests) {
    def correctos = 0
    tests.each{args, esperado ->
        try {
            def resultado = metodo(args[0])
            if (resultado == esperado) correctos++
            else {
                println "Se obtuvo <<$resultado>> y se esperaba <<$esperado>>"
            }
        }
        catch (Exception ex) {
            ex.printStackTrace()
        }
    }
    println "Pasó $correctos tests de ${tests.size()}"
}

casos = [:]
casos[[[]]] = []
casos[[new ArrayList(1..10)]] = [4,6,7,9,10]
casos[[[5,8,2,21,13,1,89]]] = []
casos[[[143,144,145]]] = [143,145]
casos[[[1000,1,987,4,2000,2584]]] = [4,1000,2000]
casos[[new ArrayList(7000..8000)]] = 7000..8000 as List

runTests({l -> filtrarFibonaccis(l)}, casos)
