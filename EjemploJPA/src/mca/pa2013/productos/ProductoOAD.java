package mca.pa2013.productos;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author jaguilar
 */
public class ProductoOAD {
    
    private EntityManager em;
    
    public ProductoOAD() {
        em = Persistence.createEntityManagerFactory("EjemploJPAPU").createEntityManager();
    }
    
    public List<Product> cargarProductos() {
        return em.createNamedQuery("Product.findAll").getResultList();
    }
    
    public List<Product> getProductosAgotados() {
        return em.createNamedQuery("Product.findByAvailable")
                .setParameter("available", false)
                .getResultList();
    }
    
    public void guardarProducto(Product producto) {
        try {
            if (producto.getManufacturerId() == null) {
                Manufacturer m = (Manufacturer) em.createNamedQuery("Manufacturer.findByManufacturerId")
                            .setParameter("manufacturerId", 19985678)
                            .getSingleResult();
                producto.setManufacturerId(m);
            }
            
            EntityTransaction et = em.getTransaction();
            et.begin();
            em.persist(producto);
            et.commit();
        }
        catch (Exception ex) {
            // TODO: Hacer algo sensato con la excepcion
            ex.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        ProductoOAD productoOad = new ProductoOAD();
        
        Product prod1 = new Product();
        prod1.setProductId(123456);
        prod1.setProductCode(new ProductCode("P1"));
//        prod1.setManufacturerId();
        prod1.setDescription("Producto de prueba 1");
        prod1.setQuantityOnHand(1);
        
        productoOad.guardarProducto(prod1);
                
        List<Product> productos = productoOad.cargarProductos();
        for (Product prod : productos) {
            System.out.println(prod);
        }
    }
}
