package mca.pa2013.clase06

def unNoveno = 1/9
println 9 * unNoveno

def otroNoveno = 1D/9
println 9 * otroNoveno

def unNovenoBD = BigDecimal.ONE/9
println 9 * unNovenoBD

def suma = 0 as BigDecimal
suma += 5.8
suma += 5.6
println suma