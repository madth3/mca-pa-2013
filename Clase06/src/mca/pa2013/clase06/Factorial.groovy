package mca.pa2013.clase06

def factorial(n) {
    if (n == 0) return BigInteger.ONE
    def f = BigInteger.ONE
    (1..n).each { f *= it}
    return f
}

println factorial(1000)