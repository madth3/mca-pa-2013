package mca.pa2013.clase06

def mezclarCadenas(r, s) {
    if (r == null || r.isEmpty()) return s
    if (s == null || s.isEmpty()) return r
    return r[0] + s[0] + mezclarCadenas(r.substring(1), s.substring(1))
}

assert mezclarCadenas("abc", "xyz") == "axbycz"
assert mezclarCadenas("Hi", "There") == "HTihere"
assert mezclarCadenas("xxxx", "There") == "xTxhxexre"
assert mezclarCadenas("xxx", "X") == "xXxx"
assert mezclarCadenas("2/", "27 around") == "22/7 around"
assert mezclarCadenas("", "Hello") == "Hello"
assert mezclarCadenas("Abc", "") == "Abc"
assert mezclarCadenas("", "") == ""
assert mezclarCadenas("a", "b") == "ab"
assert mezclarCadenas("ax", "b") == "abx"
assert mezclarCadenas("a", "bx") == "abx"
assert mezclarCadenas("So", "Long") == "SLoong"
assert mezclarCadenas("Long", "So") == "LSoong"

