package mca.pa2013.clase06

def verificarPalindromo(s) {
    for (int k = 0; k < s.size().intdiv(2); k++) {
        if (s[k] != s[s.size()-k-1]) return false
    }
    return true
}

def encontrarMayorPalindromo(s) {
    if (s.size() == 1) return s
    def mayor = ''
    for (int i = 0; i < s.size()-1; i++) {
        for (int j = i + 1; j < s.size(); j++) {
            if (verificarPalindromo(s.substring(i,j+1))) {
                if (s.substring(i, j+1).size() > mayor.size()) mayor = s.substring(i,j+1)
            }
        }
    }
    return mayor
}

//assert verificarPalindromo('')
//assert verificarPalindromo('X')
//assert verificarPalindromo('XX')
//assert verificarPalindromo('ANA ANA ANA')
//assert !verificarPalindromo('ANAA NA ANA')
//assert verificarPalindromo('CUALQUIERCOSAASOCREIUQLAUC')
//assert verificarPalindromo('ANITALAVALATINA')
//assert !verificarPalindromo('ANITALAVAKATINA')
//assert !verificarPalindromo('ZNITALAVALATINA')


assert encontrarMayorPalindromo('A') == 'A'
assert encontrarMayorPalindromo('ABCDCBCA') == 'BCDCB'
assert encontrarMayorPalindromo('ABABBABA') == 'ABABBABA'
assert encontrarMayorPalindromo('IQWYQUEYEYQTWETWUETUQYWEQYTUWTEQ').size() == 3
