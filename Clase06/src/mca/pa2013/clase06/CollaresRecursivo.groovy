package mca.pa2013.clase06

numerosCollares = [0:0, 1:0, 2:2 as BigInteger, 3:new BigInteger("3")]
//def contarCollares(N) {
//    if (N == 2 || N == 3) return N
//    return contarCollares(N-1) + contarCollares(N-2) + 1
//}
def contarCollares(N) {
    if (numerosCollares.containsKey(N)) return numerosCollares[N]
    def resultado = contarCollares(N-1) + contarCollares(N-2) + 1
    numerosCollares[N] = resultado
    return resultado
}

(2..100).each { println "$it -> ${contarCollares(it)}"}