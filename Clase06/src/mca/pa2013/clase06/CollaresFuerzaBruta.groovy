package mca.pa2013.clase06

def contarCollares(N) {
    long k
    long conteo = 0
    //def k = 1L
    def limite1 = 2**(N-1)
    for (k = 1; k < limite1; k++) {
        // Numeros que inician con 0
        def kstr = Long.toBinaryString(k)
        if (kstr.indexOf('11') == -1) {
            conteo++
        }
    }
    def limite2 = 2**N
    for (k = limite1; k < limite2; k+=2) {
        // Numeros que inician con 1
        def kstr = Long.toBinaryString(k)
        if (kstr.indexOf('11') == -1) {
            conteo++
        }        
    }
    return conteo
}


1000011000000
1000011000001
1000011000010
1000011000011



(2..30).each { println "$it ${contarCollares(it)}"}



