package mca.pa2013.quiz02

/* El metodo debe contar cuantos números positivos aparecen en la lista que se
 * entrega y que pertenecen a cada decena (1-9,10-19,20-29,etc.) 
 * Se regresará un mapa donde las llaves serán dígitos correspondientes a las 
 * decenas (0 para la decena 1-9) y los valores serán los conteos de los números.
 * Sólo deben aparecer las decenas donde existan números.
 * A un número como 673 le correspondería la decena 67
 */
def contarPorDecenas(lista) {
    return null
}


/******************************************************************************/
/**                 NO MODIFICAR DEBAJO DE ESTAS LINEAS                      **/
def runTests(metodo, tests) {
    def correctos = 0
    tests.each{args, esperado ->
        try {
            def resultado = metodo(args[0])
            if (resultado == esperado) correctos++
            else {
                println "Se obtuvo <<$resultado>> y se esperaba <<$esperado>>"
            }
        }
        catch (Exception ex) {
            ex.printStackTrace()
        }
    }
    println "Pasó $correctos tests de ${tests.size()}"
}

casos = [:]
casos[[[]]] = [:]
casos[[[1,2,14,20]]] = [0:2, 1:1, 2:1]
casos[[[61,0,20,31,43,44,]]] = [2:1, 3:1, 4:2, 6:1]
casos[[[1,2,2,4,4,4,4,8,8,8,8,8,8,8,8]]] = [0:15]
casos[[[-1]]] = [:]
casos[[[7,14,21,28,35,42,49,56,63,70,77,84,91,98]]] = [0:1, 1:1, 2:2, 3:1, 4:2, 5:1,6:1, 7:2, 8:1, 9:2]
lista = [1]*10 + [11]*11 + [21]*12 + [31]*13 +[41]*7 + [51]*6 + [61]*5 + [91]*42
Collections.shuffle(lista)
casos[[lista]] = [0:10, 1:11, 2:12, 3:13, 4:7, 5:6, 6:5, 9:42]
lista2 = lista.collect{it - 1}
casos[[lista2]] = [1:11, 2:12, 3:13, 4:7, 5:6, 6:5, 9:42]
casos[[[1234]]] = [123:1]
lista3 = lista.collect{it * it}
casos[[lista3]] = [0:10, 12:11, 44:12, 96:13, 168:7, 260:6, 372:5, 828:42]

runTests({x -> contarPorDecenas(x)}, casos)