package mca.pa2013.quiz02

/* El método debe mezclar dos mapas en uno sólo, si una llave aparece en uno de
 * los mapas entonces debe aparecer en el resultado. Si aparece en ambos mapas
 * entonces se tomará el mayor valor.
 * 
 * Todos los valores serán numéricos.
 */
def tomarValoresMayores(mapa1, mapa2) {
    return null
}


/******************************************************************************/
/**                 NO MODIFICAR DEBAJO DE ESTAS LINEAS                      **/
def runTests(metodo, tests) {
    def correctos = 0
    tests.each{args, esperado ->
        try {
            def resultado = metodo(args[0], args[1])
            if (resultado == esperado) correctos++
            else {
                println "Se obtuvo <<$resultado>> y se esperaba <<$esperado>>"
            }
        }
        catch (Exception ex) {
            ex.printStackTrace()
        }
    }
    println "Pasó $correctos tests de ${tests.size()}"
}

casos = [:]
casos[[:],[:]] = [:]
casos[[1:1],['a':67]] = [1:1, 'a':67]
casos[[1:1,2:2],[2:4,3:6]] = [1:1, 2:4, 3:6]
casos[[:],[2:4,3:6]] = [2:4,3:6]
casos[[1:1,2:2],[2:-4,3:-6]] = [1:1, 2:2, 3:-6]
casos[[1:10,2:20,3:30,4:40],[1:11,2:18,5:16]] = [1:11,2:20,3:30,4:40,5:16]
casos[[1:10,2:20,3:30,4:40],[1:10,2:20,3:30,4:40]] = [1:10,2:20,3:30,4:40]
casos[[1:10,2:20,3:30,4:40],[11:11,12:18,15:16]] = [1:10,2:20,3:30,4:40,11:11,12:18,15:16]
casos[['a':1, 'B':27, 'X':7], ["a":20, "b":28, "Equis": 10]] = ['a':20,'B':27, 'b':28, 'Equis':10, 'X':7]
def m1 = [:]
def m2 = [:]
def rnd = new Random()
('A'..'Z').each{
    m1[it] = rnd.nextInt(1000)
    m2[it] = m1[it] + rnd.nextDouble()
}
casos[m1, m2] = m2

runTests({x,y -> tomarValoresMayores(x,y)}, casos)