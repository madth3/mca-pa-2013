package mca.pa2013

/**
 *
 * @author PCEL
 */
class Quiz01 extends GroovyTestCase {
	
    /* Cuenta cuantos multiplos de k hay en la lista proporcionada
     */
    def contarMultiplos(lista, k) {
        return -1
    }
    void test01_contarMultiplos() {
        assert contarRepeticiones([1..10], 7) == 1
        assert contarRepeticiones([1..10], 3) == 3
        assert contarRepeticiones([1..10], 11) == 0
    }
    
    void test02_contarMultiplos() {
        assert contarRepeticiones([1,2,4,5,2,6,8,2], 2) == 6
        assert contarRepeticiones([1,2,4,5,2,6,8,2], 4) == 2
        assert contarRepeticiones([1,2,4,5,2,6,8,2], 1) == 8
    }
    
    /* Verifica si la diferencia entre cualesquiera dos elementos de la lista
     * es menor o igual a delta
     * Debe regresar true o false.
     */
    def esListaBalanceada(lista, delta) {
        returns 0
    }
    
    void test03_esListaBalanceada() {
        assert esListaBalanceada([1,2,3], 2) 
        assert esListaBalanceada([1,2,3], 4)
        assert !esListaBalanceada([1,2,3], 1)
    }

    void test04_esListaBalanceada() {
        assert esListaBalanceada([1,2,3,2,1], 2) 
        assert esListaBalanceada([1,2,3,2,2,1,3,4,1,3], 4)
        assert !esListaBalanceada([3,2,3,6,2,3,5,2,3,2,3,2,3,1,5,1,2,5,4], 4)
    }
    
    void test05_esListaBalanceada() {
        assert !esListaBalanceada([3,2,3,34,10,5,3,6,2,3,5,1,5,1,2,5,4], 1)
        assert !esListaBalanceada([3,2,1,3,10,5,4,5,6,8,9,2,3,3,6,2,3,5,1,5,1,2,5,12,4], 10)
    }
}

